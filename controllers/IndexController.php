<?php
/**
 * Created by PhpStorm.
 * User: fazleelahee
 * Date: 18/07/2018
 * Time: 16:50
 */

namespace Controllers;

use Helpers\GoogleMap;
use Illuminate\Support\Facades\Config;
use Models\Property;
use Models\PropertyType;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Illuminate\Database\Capsule\Manager as DB;
class IndexController
{
    /**
     * @var \Slim\Container Stores the container for dependency purposes.
     */
    protected $container;


    /**
     * Store the container during class construction.
     *
     * @param \Slim\Container $container
     */
    public function __construct(\Slim\Container $container)
    {
        $this->container = $container;
    }

    /**
     * Render home page
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function index(Request $request, Response $response, $args) {
        return $this->container->get('view')->render($response, 'home.twig', [
            'propertyTypes' => PropertyType::all()->pluck('name', 'id'),
            'apiKey' => $this->container->get('config')['google-map-api_key']
        ]);
    }

    public function propertyView(Request $request, Response $response, $args) {
        $property = Property::find($args['id']);
        $propertyArray = $property->toArray();
        $propertyArray['country'] = $property->propertyCountry->name;
        $propertyArray['property_type'] = $property->propertyType->name;

        $meta = $property->metaArray();

        if($meta['image']) {
            $propertyArray['thumbImage'] =  "/upload/images/".$meta['image'];
        }

//        var_dump($propertyArray);
//        die();
        return $this->container->get('view')->render($response, 'property-view.twig', [
            'property' => $propertyArray,

        ]);
    }
    public function geoSearch(Request $request, Response $response, $args) {
        $params = $request->getQueryParams();
        $googleMapApi = new GoogleMap($this->container);

        if(empty($params['q'])) {
            return $response->withStatus(400)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(['message'=>'Query param is empty.']));
        }

        $location = $googleMapApi->getGeoLocationByAddress($params['q']);

        $distanceQuery = DB::raw('( 3959 * acos( cos( radians('.$location->lat.') ) * cos( radians( latitude ) )
                                     * cos( radians( longitude ) - radians('.$location->lng.') ) + sin( radians('.$location->lat.') )
                                     * sin(radians(latitude)) ) ) AS distance ');
        $queryBuilder = Property::select('*', $distanceQuery)
            ->with('meta', 'propertyCountry', 'propertyType')
            ->having('distance', '<', 50)->orderBy('distance', 'ASC');

        if(!empty($params['property_for'])) {
            $queryBuilder = $queryBuilder->where('property_for', '=', $params['property_for']);
        }

        if(!empty($params['min_price']) && !empty($params['max_price']) ) {
            $queryBuilder = $queryBuilder->where(function ($query) use ($params) {
                $query->where('price', '<=', $params['max_price']);
                $query->where('price', '>', $params['min_price']);
            });
        }else if(!empty($params['min_price']) && empty($params['max_price']) ) {
            $queryBuilder = $queryBuilder->where('price', '>', $params['min_price']);
        } else if(empty($params['min_price']) && !empty($params['max_price']) ) {
            $queryBuilder = $queryBuilder->where('price', '<=', $params['max_price']);
        }

        if(!empty($params['property_type'])) {
            $queryBuilder = $queryBuilder->where('property_type', '=', $params['property_type']);
        }
        $properties = $queryBuilder->get();
        $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($properties));
    }
}