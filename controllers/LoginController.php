<?php
/**
 * Created by PhpStorm.
 * User: fazleelahee
 * Date: 18/07/2018
 * Time: 16:50
 */

namespace Controllers;

use Models\User;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class LoginController
{
    /**
     * @var \Slim\Container Stores the container for dependency purposes.
     */
    protected $container;


    /**
     * Store the container during class construction.
     *
     * @param \Slim\Container $container
     */
    public function __construct(\Slim\Container $container)
    {
        $this->container = $container;
    }

    /**
     * Render home page
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function index(Request $request, Response $response, $args) {
        $error =  '' ;
        if(isset($_SESSION['error'])) {
            $error = $_SESSION['error'];
            unset($_SESSION['error']);
        }
        return $this->container->get('view')->render($response, 'login.twig', [
            'error' => $error
        ]);
    }

    public function login(Request $request, Response $response, $args) {
        $params = $request->getParsedBody();
        $email = isset($params['email']) ?  filter_var($params['email'], FILTER_SANITIZE_EMAIL) : '';
        $password =  isset($params['password']) ? $params['password'] : '';
        if(User::validate($email, $password)) {
            header("Location: /admin/dashboard");
            exit;
        }

        $_SESSION['error'] = "Invalid email/ password entered.";
        header("Location: /login");
        exit;
    }

    public function logout() {
        session_destroy();
        header("Location: /");
        exit;
    }
}