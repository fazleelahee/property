<?php
namespace Controllers\Admin;

use Models\Property;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Xandros15\SlimPagination\Pagination;

class DashboardController
{
    /**
     * @var \Slim\Container Stores the container for dependency purposes.
     */
    protected $container;


    /**
     * Store the container during class construction.
     *
     * @param \Slim\Container $container
     */
    public function __construct(\Slim\Container $container)
    {
        $this->container = $container;
    }

    public function index(Request $request, Response $response, $args)
    {
        return $this->container->get('view')->render(
            $response, 'admin.dashboard.twig', [
                'user' => $this->container->get('user')
            ]
        );
    }
}