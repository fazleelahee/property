<?php

namespace Controllers\Admin;

use Property\Api\Zoopla;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ZooplaController
{
    /**
     * @var \Slim\Container Stores the container for dependency purposes.
     */
    protected $container;


    /**
     * Store the container during class construction.
     *
     * @param \Slim\Container $container
     */
    public function __construct(\Slim\Container $container)
    {
        $this->container = $container;
    }

    public function index(Request $request, Response $response, $args)
    {
        $error = '';
        $success = '';

        if(isset($_SESSION['error'])) {
            $error = $_SESSION['error'];
            unset($_SESSION['error']);
        }

        if(isset($_SESSION['success'])) {
            $success= $_SESSION['success'];
            unset($_SESSION['success']);
        }

        return $this->container->get('view')->render(
            $response, 'admin.zoopla.twig', [
                'error' => $error,
                'success' => $success
            ]
        );
    }

    public function import(Request $request, Response $response, $args) {
        $params = $request->getParsedBody();
        $area = explode('|',$params['area']);
        $zoopla = new Zoopla($this->container);

        try {

            $zoopla->importProperties($area);
            $_SESSION['success'] = implode(' | ', $area)." successfully imported";;
            header("Location: /admin/property/zoopla");
        } catch (\Exception $e) {
            $_SESSION['error'] = $e->getMessage();
            header("Location: /admin/property/zoopla");
        }

        exit;
    }
}