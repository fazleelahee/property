<?php
namespace Controllers\Admin;

use Gumlet\ImageResize;
use Helpers\GoogleMap;
use Illuminate\Pagination\Paginator;
use Models\Country;
use Models\Property;
use Models\PropertyMeta;
use Models\PropertyType;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Http\UploadedFile;
use Valitron\Validator;

class PropertyController
{
    /**
     * @var \Slim\Container Stores the container for dependency purposes.
     */
    protected $container;


    /**
     * Store the container during class construction.
     *
     * @param \Slim\Container $container
     */
    public function __construct(\Slim\Container $container)
    {
        $this->container = $container;
    }

    public function index(Request $request, Response $response, $args)
    {
        $params = $request->getQueryParams();
        $draw = isset($params['draw']) ? (int) $params['draw'] : 1;
        $start = isset($params['start']) ? (int) $params['start'] : 0;
        $length = isset($params['length'])? (int) $params['length'] : 10;
        $querytxt = !empty($params['search']['value']) ? filter_var($params['search']['value'], FILTER_SANITIZE_STRING) : false;
        $currentPage = $start > 0 ? ($start/$length)+1 : 1; // You can set this to any page you want to paginate to
        // Make sure that you call the static method currentPageResolver()
        // before querying users
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        if($querytxt) {
            $properties = Property::where( function ($query) use($querytxt) {
                $query->where('displayable_address', 'LIKE', "%{$querytxt}%")
                ->orWhere('town', 'LIKE', "%{$querytxt}%");
            })->where('soft_delete', '<>', true)->orderBy('id', 'desc')->paginate($length);
        } else {
            $properties = Property::where('soft_delete', '<>', true)->orderBy('id', 'desc')->paginate($length);
        }


        $propertiesJson = json_decode(json_encode($properties));
        $output = [
            'data' =>[],
            'draw' =>  (int) $draw,
            'recordsFiltered' => $propertiesJson->total,
            'recordsTotal' => $propertiesJson->total
        ];


        foreach($properties as $property) {
            $tmp = [];
            $tmp[] = $property->id;
            $tmp[] = $property->displayable_address;
            $tmp[] = $property->town;
            $tmp[] = $property->number_of_bedrooms;
            $tmp[] = isset($property->propertyType->name) ? $property->propertyType->name : 'Unknown';
            $tmp[] = $property->property_for;
            $tmp[] = $property->price;
            $tmp[] = $property->propertySource->name;

            $output['data'][] = $tmp;
        }
        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($output));
    }

    public function add(Request $request, Response $response, $args) {
        $error = '';
        if(isset($_SESSION['error'])) {
            $error = $_SESSION['error'];
            unset($_SESSION['error']);
        }
        $old = [];
        if(isset($_SESSION['old_params'])) {
            $old = $_SESSION['old_params'];
            unset($_SESSION['old_params']);
        }
        return $this->container->get('view')->render(
            $response, 'admin.property-add.twig', [
                'user' => $this->container->get('user'),
                'propertyTypes' => PropertyType::all()->pluck('name', 'id'),
                'error' => $error,
                'old' => $old
            ]
        );
    }
    public function edit(Request $request, Response $response, $args) {
        $error = '';
        if(isset($_SESSION['error'])) {
            $error = $_SESSION['error'];
            unset($_SESSION['error']);
        }
        $property = Property::find($args['id']);

        $old = $property->toArray();
        $old['image'] = '';
        foreach( $property->metaArray() as $key=>$value) {
            $old[$key] = $value;
        }


        $old['country'] = $property->propertyCountry->name;

        return $this->container->get('view')->render(
            $response, 'admin.property-edit.twig', [
                'user' => $this->container->get('user'),
                'thumb_path' => $this->container->get('config')['image_upload'].'thumb/',
                'propertyTypes' => PropertyType::all()->pluck('name', 'id'),
                'error' => $error,
                'old' => $old
            ]
        );
    }
    public function store(Request $request, Response $response, $args) {
        $params = $request->getParsedBody();
        $_SESSION['old_params'] = $params;

        $validator = new Validator($params);
        $validator->rule('required', ['country', 'county', 'town', 'displayable_address', 'description', 'number_of_bedrooms',
            'number_of_bathrooms', 'price', 'property_type', 'property_for', 'postcode']);

        if(!$validator->validate()) {
            $_SESSION['error'] = "";
            foreach($validator->errors() as $error) {
                $_SESSION['error'] .= $error[0]."<br />";
            }
            header("Location: /admin/property/new");
            exit;
        }

        $params['source'] = 1;
        $googleMap = new GoogleMap($this->container);
        $location = $googleMap->getGeoLocationByAddress(implode(',',[
            $params['displayable_address'],
            $params['postcode']
        ]));

        if($location) {
            $params['latitude'] = $location->lat;
            $params['longitude'] = $location->lng;
        }
        $countryName = strtolower($params['country']);
        $country = Country::whereRaw("LOWER(name) LIKE '%{$countryName}%'")->first();

        if(!$country) {
            $_SESSION['error'] = "Invalid country name entered";
            header("Location: /admin/property/new");
            exit;
        }
        $params['country'] = $country->id;

        $property = new Property();
        $property->fill($params)->save();
        //store post code to meta
        PropertyMeta::create([
            'property_id' => $property->id,
            'meta_key' => 'postcode',
            'meta_value' => $params['postcode']
        ]);

        $uploadPath = $this->container->get('config')['image_upload'];
        $uploadedFiles = $request->getUploadedFiles();

        // handle single input with single file upload
        $uploadedFile = $uploadedFiles['image'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = $this->moveUploadedFile($uploadPath, $uploadedFile);
            //store post code to meta
            PropertyMeta::create([
                'property_id' => $property->id,
                'meta_key' => 'image',
                'meta_value' => $filename
            ]);

            //generate thumb image
            $image = new ImageResize($uploadPath.$filename);
            $image->resize(150, 150);
            $image->save($uploadPath.'thumb/'.$filename);
        }
        $_SESSION['success'] = "Property save successfully";
        header("Location: /admin/property/{$property->id}/edit");
        exit;
    }

    public function update(Request $request, Response $response, $args) {
        $params = $request->getParsedBody();

        $property = Property::find($args['id']);

        $validator = new Validator($params);
        $validator->rule('required', ['country', 'county', 'town', 'displayable_address', 'description', 'number_of_bedrooms',
            'number_of_bathrooms', 'price', 'property_type', 'property_for', 'postcode']);

        if(!$validator->validate()) {
            $_SESSION['error'] = "";
            foreach($validator->errors() as $error) {
                $_SESSION['error'] .= $error[0]."<br />";
            }
            header("Location: /admin/property/{$property->id}/edit");
            exit;
        }

        $params['source'] = 1;
        $googleMap = new GoogleMap($this->container);
        $location = $googleMap->getGeoLocationByAddress(implode(',',[
            $params['displayable_address'],
            $params['postcode']
        ]));

        if($location) {
            $params['latitude'] = $location->lat;
            $params['longitude'] = $location->lng;
        }
        $countryName = trim(strtolower($params['country']));
        $country = Country::whereRaw("LOWER(name) LIKE '%{$countryName}%'")->first();

        if(!$country) {
            $_SESSION['error'] = "Invalid country name entered";
            header("Location: /admin/property/{$property->id}/edit");
            exit;
        }
        $params['country'] = $country->id;

        $property->fill($params)->save();
        //store post code to meta
        $postcodeMeta = PropertyMeta::where([
            ['property_id', '=', $property->id],
            ['meta_key', '=', 'postcode',]
        ])->first();

        if(!$postcodeMeta) {
            PropertyMeta::create([
                'property_id' => $property->id,
                'meta_key' => 'postcode',
                'meta_value' => $params['postcode']
            ]);
        } else {
            $postcodeMeta->meta_value = $params['postcode'];
            $postcodeMeta->save();
        }


        $uploadPath = $this->container->get('config')['image_upload'];
        $uploadedFiles = $request->getUploadedFiles();

        // handle single input with single file upload
        $uploadedFile = $uploadedFiles['image'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $imageMeta = PropertyMeta::where([
                ['property_id', '=', $property->id],
                ['meta_key', '=', 'image',]
            ])->first();

            if($imageMeta) {
                if(file_exists($uploadPath.'thumb/'.$postcodeMeta->meta_value)) {
                    unlink($uploadPath.'thumb/'.$imageMeta->meta_value);
                }
                if(file_exists($uploadPath.$postcodeMeta->meta_value)) {
                    unlink($uploadPath.$imageMeta->meta_value);
                }
                $imageMeta->delete();
            }

            $filename = $this->moveUploadedFile($uploadPath, $uploadedFile);
            //store post code to meta
            PropertyMeta::create([
                'property_id' => $property->id,
                'meta_key' => 'image',
                'meta_value' => $filename
            ]);

            //generate thumb image
            $image = new ImageResize($uploadPath.$filename);
            $image->resize(150, 150);
            $image->save($uploadPath.'thumb/'.$filename);
        }
        $_SESSION['success'] = "Property update successfully";
        header("Location: /admin/property/{$property->id}/edit");
        exit;
    }

    public function delete(Request $request, Response $response, $args) {
        $property = Property::find($args['id']);

        if(!$property) {
            return $response->withStatus(404)
                ->withHeader('Content-Type', 'application/json')
                ->write(json_encode(['message'=>'Property not found']));
        }

        if($property->propertySource->name == 'zoopla') {
            $property->soft_delete = true;
            $property->save();
        } else {
            $uploadPath = $this->container->get('config')['image_upload'];
            foreach($property->productMeta as $meta) {
                if($meta->meta_key == 'image') {
                    if(file_exists($uploadPath.'thumb/'.$meta->meta_value)) {
                        unlink($uploadPath.'thumb/'.$meta->meta_value);
                    }
                    if(file_exists($uploadPath.$meta->meta_value)) {
                        unlink($uploadPath.$meta->meta_value);
                    }
                }
                $meta->delete();
            }
            $property->delete();
        }

        return $response->withStatus(204)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(['message'=>'Property has been deleted successfully.']));
    }

    protected function moveUploadedFile($directory, UploadedFile $uploadedFile)
    {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
        $filename = sprintf('%s.%0.8s', $basename, $extension);
        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
        return $filename;
    }
}

