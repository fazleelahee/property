<?php

return [
    'slim' => [
        'settings' => [
            // Slim Settings
            'determineRouteBeforeAppMiddleware' => false,
            'displayErrorDetails' => true,
            'db' => [
                'driver' => 'mysql',
                'host' => 'localhost',
                'database' => 'property',
                'username' => 'root',
                'password' => 'root',
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
            ]
        ],
    ],
    'image_upload'=> 'upload/images/',
    'zoopla' => [
        'api_key' => 'raqjr53tyfbdytqt8bc7r3h8',
        'listing_endpoint' => 'http://api.zoopla.co.uk/api/v1/property_listings.json',
        'default_location' => ['Milton Keynes', 'stratford, London', 'central london', 'Dundee', 'Edinburgh']
    ],

    'google-map-api_key' => 'AIzaSyDq3wO4riWDnNDSSUL9NhE_2dYj5DXaiPU',
    'google-map-api-endpoint'=> 'https://maps.googleapis.com/maps/api/geocode/json'
];