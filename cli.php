<?php
require __DIR__ . '/vendor/autoload.php';

if (PHP_SAPI == 'cli') {
    $argv = $GLOBALS['argv'];
    array_shift($argv);

    $pathInfo       = implode('/', $argv);

    $env =  \Slim\Http\Environment::mock(['REQUEST_URI' => '/' . $pathInfo]);

    $settings = require __DIR__ . '/config.php'; // here are return ['settings'=>'']

    //I try adding here path_info but this is wrong, I'm sure
    $settings['environment'] = $env;

    $app = new \Slim\App($settings);

    $container = $app->getContainer();
    $container['errorHandler'] = function ($c) {
        return function ($request, $response, $exception) use ($c) {
            //this is wrong, i'm not with http
            return $c['response']->withStatus(500)
                ->withHeader('Content-Type', 'text/text')
                ->write('Something went wrong!');
        };
    };

    $container['notFoundHandler'] = function ($c) {
        //this is wrong, i'm not with http
        return function ($request, $response) use ($c) {
            return $c['response']
                ->withStatus(404)
                ->withHeader('Content-Type', 'text/text')
                ->write('Not Found');
        };
    };

    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($settings["slim"]['settings']['db']);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    //register eloquant ORM with slim
    $container['db'] = function () use ($capsule) {
        return $capsule;
    };

    $container['config'] = function () use ($settings) {
        return $settings;
    };

    $commands = include_once "console/kernel.php";
    if(array_key_exists($argv[0], $commands)) {
        $commandKey = $argv[0];
        unset($argv[0]);
        $command = new $commands[$commandKey]($app, $container, $argv);
        $command->handle();
    }
}