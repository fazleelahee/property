<?php

// Render Twig template within container
// Render home page
$app->get('/', 'Controllers\\IndexController:index');
$app->get('/property/search', 'Controllers\\IndexController:geoSearch');
$app->get('/property/{id:[0-9]+}/view', 'Controllers\\IndexController:propertyView');

$app->get('/login', 'Controllers\\LoginController:index')->add(function ($request, $response, $next) use ($container) {
    if($container->get('user') instanceof \Models\User) {
        header('Location: /admin/dashboard');
        exit;
    }
    return $next($request, $response);
});

$app->post('/login', 'Controllers\\LoginController:login');

$app->group('/admin', function () use($app) {
    $app->post('/logout', 'Controllers\\LoginController:logout');
    $app->get('/dashboard', 'Controllers\\Admin\\DashboardController:index');
    $app->get('/property/listing', 'Controllers\\Admin\\PropertyController:index');
    $app->delete('/property/{id:[0-9]+}/delete', 'Controllers\\Admin\\PropertyController:delete');
    $app->get('/property/{id:[0-9]+}/edit', 'Controllers\\Admin\\PropertyController:edit');
    $app->post('/property/{id:[0-9]+}/edit', 'Controllers\\Admin\\PropertyController:update');
    $app->get('/property/new', 'Controllers\\Admin\\PropertyController:add');
    $app->post('/property/new', 'Controllers\\Admin\\PropertyController:store');

    $app->get('/property/zoopla', 'Controllers\\Admin\\ZooplaController:index');
    $app->post('/property/zoopla', 'Controllers\\Admin\\ZooplaController:import');

})->add(function ($request, $response, $next) use ($container) {
    if($container->get('user') instanceof \Models\User) {
        return $next($request, $response);
    }
    header('Location: /login');
    exit;
});


//$app->get('/hello/{name}', 'Controllers\\HelloController:index');
