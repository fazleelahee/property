<?php
session_cache_limiter(false);
session_start();

require_once '../vendor/autoload.php';
require_once '../bootstrap.php';
require_once '../routes.php';

// Run app
$app->run();
