(function ($) {
    $(document).ready(function()  {
        $('.sign-out').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '/admin/logout',
                method: 'post'
            }).done(function(response) {
                window.location.href="/login";
            });
        });

        var table = $('.property-listing').DataTable( {
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "ajax": '/admin/property/listing',
            "columnDefs": [{
                "targets": -1,
                "data": null,
                "defaultContent": "<button class='edit-row btn btn-secondary'>Edit</button>"
            }, {
                    "targets": -2,
                    "data": null,
                    "defaultContent": "<button class='delete-row btn btn-danger'>Delete</button>"
                },
            ]
        } );

        $('.property-listing').on('click', '.delete-row', function (e) {
            e.preventDefault();
            var response = confirm("Are you sure you want to delete property");
            if(response) {
                var propertyid = $(this).closest('tr').find('td:first-child').text();
                $.ajax({
                    url: "/admin/property/"+propertyid+"/delete",
                    data: {
                        _method: 'delete'
                    },
                    method: 'delete',
                    success: function (response) {
                        table.draw(false);
                        alert('Property has been deleted successfully.');
                    }
                });
            }
        });

        $('.property-listing').on('click', '.edit-row', function (e) {
            e.preventDefault();
            var propertyid = $(this).closest('tr').find('td:first-child').text();
            var url = "/admin/property/"+propertyid+"/edit";
            window.location.href=url;
        });

        /** Property Save **/
        $.validator.addMethod('Decimal', function(value, element) {
            return this.optional(element) || /^[0-9,]+(\.\d{0,3})?$/.test(value);
        }, "Please enter a correct number, format xxxx.xxx");

        $("#property-form").validate({
            rules: {
                price: { Decimal : true}
            }
        });
    });
})(jQuery);