(function ($) {

    $(document).ready(function () {
        $('#name').show();
        $('#companyName').hide();

        $('select#userType').change(function (e) {
            e.preventDefault();
            if($(this).val() == 'candidate') {
                $('#name').show();
                $('#companyName').hide();
            } else {
                $('#name').hide();
                $('#companyName').show();
            }
        });


    });
})(jQuery)
