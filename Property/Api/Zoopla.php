<?php
namespace Property\Api;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\Request;
use Models\Country;
use Models\Property;
use Models\PropertyMeta;
use Models\PropertySource;
use Models\PropertyType;

/**
 * Access Zoopla api and import properties from zoopla
 * Class Zoopla
 * @package Property\Api
 */
class Zoopla
{
    protected $client;
    protected $container;
    protected $total = 0;
    protected $rejected = 0;

    /**
     * @param $container
     */
    public function __construct($container) {
        $this->client = new Client();
        $this->container = $container;
    }

    /**
     * Import properties from zoopla
     * @param array $area
     * @param int $pageNumber
     * @param bool|true $initial
     * @param int $totalItems
     */
    public function importProperties(array $area=[], $pageNumber = 1, $initial = true, $totalItems = 0) {

        if($initial) {
            /**
             * create a aync request
             * @return \Generator
             */
            $requests = function () use($area, $pageNumber) {
                foreach($area as $val) {
                    $queryStrArray['area'] = $val;
                    $queryStrArray['api_key'] = $this->container->get('config')['zoopla']['api_key'];
                    $queryStrArray['page_size'] = 100;
                    $queryStrArray['page_number'] = $pageNumber;
                    $endPoint = $this->container->get('config')['zoopla']['listing_endpoint'].'?'.http_build_query($queryStrArray);
                    yield new Request('GET', $endPoint);
                }
            };
        } else {
            $requests = function () use($area, $pageNumber, $totalItems) {
                $totalPages = ceil($totalItems / 100);
                echo "total pages: {$totalPages} \n";
                for($i=2; $i <= $totalPages; $i++) {
                    $queryStrArray['area'] = $area[0];
                    $queryStrArray['api_key'] = $this->container->get('config')['zoopla']['api_key'];
                    $queryStrArray['page_size'] = 100;
                    $queryStrArray['page_number'] = $i;
                    $endPoint = $this->container->get('config')['zoopla']['listing_endpoint'].'?'.http_build_query($queryStrArray);
                    echo "$endPoint \n";
                    yield new Request('GET', $endPoint);
                }
            };
        }


        $pool = new Pool($this->client, $requests(), [
            'concurrency' => 5,
            'fulfilled' => function ($response, $index) use ($initial, $area)  {
                $properties = json_decode((string) $response->getBody());
                $this->processResponse($properties);
                echo "\nTotal Result Count: {$properties->result_count}\n";
                /**
                 * Zoopla listing api max number of poperties return 100, if more properties morethan
                 * 100, API will call recursively to fetch remaining properties.
                 */
                if($initial && $properties->result_count > 100) {
                    echo "\nArea Name: {$area[$index]}\n";
                    echo "\nResult Count: {$properties->result_count}\n";
                    $this->importProperties([0=>$area[$index]],2, false, $properties->result_count);
                }
            },
            'rejected' => function ($reason, $index) {
                throw new \Exception($reason->getMessage());
            },
        ]);

        // Initiate the transfers and create a promise
        $promise = $pool->promise();

        // Force the pool of requests to complete.
        $promise->wait();

    }

    /**
     * process response and stored into database.
     * @param $properties
     */
    protected function processResponse($properties) {
        if(!empty($properties->listing)) {
            foreach($properties->listing as $property) {
                $propertyArray = $this->objectToPropertyArray($property);
                $this->total++;
                if(isset($propertyArray['property'])) {
                    $property = $this->storeProperty($propertyArray['property']);
                    if(isset($property->id)) {
                        $this->storePropertyMeta($property, $propertyArray['meta']);
                    }
                }
            }
        }// end of storing


    }

    protected function objectToPropertyArray($object) {
        $propertyType = PropertyType::getPropertyTypeByName($object->property_type);

        if(!$propertyType) {
            if($object->property_type == 'Detached bungalow') {
                $propertyType = PropertyType::getPropertyTypeByName('Bungalow');
            }else if(trim($object->property_type) == '') {
                $propertyType = PropertyType::getPropertyTypeByName('Unknown');
            } else {
                $propertyType = PropertyType::getPropertyTypeByName(str_replace('house', '', $object->property_type));
            }

            if(!$propertyType && trim($object->property_type) != '') {
                $propertyType = new PropertyType();
                $propertyType->fill([
                    'name' => $object->property_type
                ])->save();
            }

        }

        $output = [
            'property' => [
                'country' => Country::getCountryByCode($object->country_code)->id,
                'county' => $object->county,
                'town' => $object->post_town,
                'displayable_address' => $object->displayable_address,
                'description'=>$object->description,
                'number_of_bedrooms' => $object->num_bedrooms,
                'number_of_bathrooms' => $object->num_bathrooms,
                'price' => $object->price,
                'property_type' => $propertyType->id,
                'property_for' => $object->listing_status,
                'latitude'=>$object->latitude,
                'longitude' => $object->longitude,
                'source' => PropertySource::getSourceByName('zoopla')->id,
                'external_reference' => $object->listing_id
            ],
            'meta' => []
        ];

        $meta_fields = explode(',', PropertySource::getSourceByName('zoopla')->meta_fields);
        foreach($meta_fields as $meta_key) {
            if(property_exists($object, $meta_key)) {
                $output['meta'][$meta_key] = $object->{$meta_key};
            }
        }

        return $output;
    }

    protected function storeProperty(array $propertyArray) {
        $property = Property::where('external_reference', '=', $propertyArray['external_reference'])->first();
        if(!$property) {
            $property = new Property();
        }
        $property->fill($propertyArray)->save();
        return $property? $property : null;
    }

    protected function storePropertyMeta(Property $property, array $meta) {
        if($property->meta()->count() > 0 ) {
           foreach($property->meta as $_meta) {
               if(array_key_exists($_meta->meta_key, $meta)) {
                   $_meta->meta_value = $meta[$_meta->meta_key];
                   $_meta->save();
                   unset($meta[$_meta->meta_key]);
               }
           }
        } else {
            foreach($meta as $key=>$val) {
                PropertyMeta::create([
                    'property_id' => $property->id,
                    'meta_key' => $key,
                    'meta_value' => $val
                ]);
            }

            unset($meta);
        }

        if(isset($meta) && count($meta) > 0) {
            foreach($meta as $key=>$val) {
                PropertyMeta::create([
                    'property_id' => $property->id,
                    'meta_key' => $key,
                    'meta_value' => $val
                ]);
            }
        }
    }
}