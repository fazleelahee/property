## Tech Test for MTC Media

### How do I get started?

Take a copy of `config_sample.php` and rename to `config.php`, entering information about your MySQL connection.

You'll also need to run `composer install` to setup necessary assets to get started.

### Update property from Zoopla
By default from frontend property search request - properties data response from the database cache. We can update update database cache running following command in command line:

```
$php cli.php  zoopla Dundee Edinburgh
```
**Note** Currently my zoopla developer key is inactive. in order to make this work - you need to update active zoopla api in the config.php

### Showing old property information - how we can see updated property listing?

If you run the above command it will update the latest property information the database. But its does require manually run this command to update property information. We can automate this by adding this command in the cron job and execute this every night.

## Demo

* URL: http://fazle.trial.mtcdevserver3.com/
* Admin URL: http://fazle.trial.mtcdevserver3.com/login