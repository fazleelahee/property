<?php
/**
 * Created by PhpStorm.
 * User: fazleelahee
 * Date: 18/07/2018
 * Time: 22:17
 */

namespace Models;

use Illuminate\Database\Eloquent\Model;

class PropertySource extends Model
{
    protected $table="properties_source";

    public static function getSourceByName($name) {
        return PropertySource::where('name', '=',$name )->first();
    }
}