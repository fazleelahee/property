<?php
namespace Models;
use Illuminate\Database\Eloquent\Model;

class PropertyMeta extends Model
{
    protected $table="properties_meta";

    /**
     * only allowed field inserted to the tabled.
     * @var array
     */
    protected $fillable = ['property_id', 'meta_key', 'meta_value'];

}