<?php
namespace Models;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = "properties";

    //only allowed field inserted to the tabled.
    protected $fillable = [ 'country', 'county', 'town', 'displayable_address', 'description', 'number_of_bedrooms',
        'number_of_bathrooms', 'price', 'property_type', 'property_for', 'latitude', 'longitude',
        'source', 'external_reference', 'soft_delete' ];

    /**
     * get all meta fields belong to property
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function meta()
    {
        return $this->hasMany(PropertyMeta::class, 'property_id', 'id');
    }

    /**
     * get meta fields belong to property as array.
     * @return array
     */
    public function metaArray() {
        $output = [];
        $metaCollection = $this->meta;
        foreach($metaCollection as $meta) {
            $output[$meta->meta_key] = $meta->meta_value;
        }
        return $output;
    }

    /**
     * Get single meta belong to property by key
     * @param $key
     * @return mixed
     */
    public function getMetaByKey($key) {
        return $this->meta->where('meta_key','=', $key )->first();
    }

    /**
     * Get a property type object belong to property
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function propertyType() {
        return $this->hasOne(PropertyType::class, 'id', 'property_type');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function propertySource() {
        return $this->hasOne(PropertySource::class, 'id', 'source');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function propertyCountry() {
        return $this->hasOne(Country::class, 'id', 'country');
    }

    /**
     * Title mutator to generate title field.
     * @return string
     */
    public function getTitleAttribute() {
        return $this->number_of_bedrooms.' '.$this->propertyType->name. ','.$this->town;
    }
}