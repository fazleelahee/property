<?php
/**
 * Created by PhpStorm.
 * User: fazleelahee
 * Date: 18/07/2018
 * Time: 14:48
 */

namespace Models;
use Illuminate\Database\Eloquent\Model;

class PropertyType extends Model
{
    protected $table="properties_types";
    protected $fillable = ['name'];
    public static function getPropertyTypeByName($name) {
        return PropertyType::where('name', '=', $name )->first();
    }
}