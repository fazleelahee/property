<?php
/**
 * Created by PhpStorm.
 * User: fazleelahee
 * Date: 18/07/2018
 * Time: 16:10
 */

namespace Models;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "users";
    /**
     * Hide password from user object.
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * Validate user and stored user object to session if validation successful.
     * @param $email
     * @param $password
     * @return bool
     */
    public static function validate($email, $password) {
        $password = md5($password);
        $user = User::where([
            [ 'email', '=', $email],
            ['password', '=', $password]
        ])->first();
        if($user) {
            $_SESSION['user'] = serialize($user);
            return true;
        }
        return false;
    }
}