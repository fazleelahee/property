<?php
/**
 * Created by PhpStorm.
 * User: fazleelahee
 * Date: 19/07/2018
 * Time: 00:50
 */
namespace Models;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = "country";

    /**
     * Get country object by country code. i.e. gb
     * @param $code
     * @return mixed
     */
    public static function getCountryByCode($code) {
        return Country::where('country_code', '=', $code)->first();
    }

    /**
     * Get country object by country name. i.e. United Kingdom or Great Britain
     * @param $name
     * @return mixed
     */
    public static function getCountryByName($name) {
        return Country::where('name', '=', $name)->first();
    }
}