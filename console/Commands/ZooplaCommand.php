<?php
namespace Commands;

use Commands\Contracts\ICommands;
use Models\Property;
use Property\Api\Zoopla;
use Slim\Container;

class ZooplaCommand implements ICommands
{
    protected $container;
    protected $app;
    protected $args;

    public function __construct($app, Container $container, array $args=[]) {
        $this->container = $container;
        $this->app = $app;
        $this->args = $args;
    }

    public function handle() {
        $area = [];
        foreach($this->args as $val) {
            $area[] = htmlspecialchars_decode($val);
        }

        if(count($area) == 0) {
            $area = ['Edinburgh'];
        }

        try {
            $zooplaApi = new Zoopla($this->container);
            $zooplaApi->importProperties($area);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

    }
}