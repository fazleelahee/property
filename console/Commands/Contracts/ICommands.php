<?php
namespace Commands\Contracts;

interface ICommands
{
    public function __construct($app, \Slim\Container $container, array $args=[]);
    public function handle();
}