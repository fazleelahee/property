<?php
namespace Commands;

use Commands\Contracts\ICommands;
use Models\Property;
use Slim\Container;

class HelloWorld implements ICommands
{
    protected $container;
    protected $app;

    public function __construct($app, Container $container) {
        $this->container = $container;
        $this->app = $app;
    }

    public function handle() {
        $property = Property::find(1);
        print_r($property);
    }
}