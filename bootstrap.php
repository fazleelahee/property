<?php

// Load up the config and create our application.
$config = include_once 'config.php';
$container = new \Slim\Container($config['slim']);
$app = new \Slim\App($container);

// Register view component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(
        '../templates', [
        'cache' => false
        ]
    );

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container->get('request')->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container->get('router'), $basePath));

    return $view;
};

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($config["slim"]['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

//register eloquant ORM with slim
$container['db'] = function () use ($capsule) {
    return $capsule;
};

$container['user'] = function ()  {
    if(isset($_SESSION['user'])) {
        $user = unserialize($_SESSION['user']);
        if($user instanceof \Models\User) {
            return $user;
        }
    }
    return null;
};

$container['config'] = function () use ($config) {
    return $config;
};
