<?php
namespace Helpers;

use GuzzleHttp\Client;

/**
 * Class GoogleMap
 * @package Helpers
 */
class GoogleMap
{
    protected $client;
    protected $container;
    public function __construct($container)
    {
        $this->client = new Client();
        $this->container = $container;
    }

    /**
     * Get Latitude and Longitude by address
     * @param $address
     * @return null
     */
    public function getGeoLocationByAddress($address) {
        $query['key'] = $this->container->get('config')['google-map-api_key'];
        $query['address'] = $address;
        //generate url
        $url = $this->container->get('config')['google-map-api-endpoint']."?".http_build_query($query);
        $response = $this->client->request('GET', $url);
        $geoData = json_decode((string)$response->getBody());
        if(isset($geoData->results[0]->geometry->location)) {
            return $geoData->results[0]->geometry->location;
        }

        return null;
    }
}